# TODOs:
- HPA demo

# Kubernetes

## Slides
https://docs.google.com/presentation/d/16ctEgENdXlJs2ymSDo-bPwQTkShJGxVzeTcw-4VM9-k

### kubectl
```sql
wget https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubectl
```
Docs: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands

### Kontena Lens
https://www.kontena.io/

### Kubernetes docs
- components: https://kubernetes.io/docs/concepts/overview/components/
- API: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/

### Ingress annotations
https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/

### Helm
- https://helm.sh/
- charts: https://github.com/helm/charts/tree/master/stable


## Work with test app
- `kubectl apply -f k8/app.yml`
- `kubectl rollout restart deployment/test-app`
- `kubectl rollout status deployment/test-app`
- `kubectl delete -f k8/app.yml`

## Aliases
```bash

kube_nodes() {
    PODS=$(k get pods -o wide)
    NODES=$(k get nodes --no-headers | awk '{print $1}' | grep -v "master")
    for NODE_NAME in $NODES
    do
        NODE_PODS=$(echo "$PODS" | grep "$NODE_NAME")
        TOTAL_COUNT=$(echo "$NODE_PODS" | wc -l)
        RUNNING_COUNT=$(echo "$NODE_PODS" | grep -i running | wc -l)
        PENDING_COUNT=$(echo "$NODE_PODS" | grep -iv running | grep -iv completed | grep -iv terminating | wc -l)
        COMPLETED_COUNT=$(echo "$NODE_PODS" | grep -i completed | wc -l)
        TERMINATED_COUNT=$(echo "$NODE_PODS" | grep -i terminating | wc -l)
        echo "Worker $NODE_NAME:"
        echo " - Pods: $TOTAL_COUNT"
        echo "   - Running: $RUNNING_COUNT"
        echo "   - Pending: $PENDING_COUNT"
        echo "   - Completed: $COMPLETED_COUNT"
        echo "   - Terminated: $TERMINATED_COUNT"
        echo ""
    done
}

alias kube_pending="k get pods -o wide | grep -iv running | grep -iv completed | grep -iv terminating"
alias kube_terminating="k get pods -o wide | grep -i terminating"
```
