IMAGE_NAME="kluvi/k8-test:latest"
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

docker build --tag $IMAGE_NAME --file $SCRIPTPATH/Dockerfile $SCRIPTPATH
docker push $IMAGE_NAME
